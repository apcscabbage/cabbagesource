# This is a flowchart/general high level representation of the way the CABBAGE system will work

#						E-Mail Notification Class
#	This class is the main form in which to communicate information to the user.
#	It will serve to notify the user if their automobile has been in an accident,
#	and will communicate specific details about the accident to the user. These specific
#	details include:
#		-Speed the vehicle was going prior to the accident
#		-Location, date, and time of the accident (?)
#		-The force with which the vehicle was hit
#		-Where on the vehicle damages likely have occurred
#		-Status of key systems in the vehicle (To be specified later)
# 	Status: Basic message system integrated, needs to connect with other classes and implement above details
#
#						Car Internal Information Class
#	This class will serve as the main form of interacting with the car's OBD-II port to gather vital information 
#	about the state of the vehicle and its systems before and after an accident.
#	Information to be collected:
#		-Speed of the vehicle prior to the accident
#		-Status of key systems within the car (To be specified later)
#	This class will be responsible for collecting and reporting this data to the main email notificaton class
#	Status: API selected, to be implemented...
#
#						Car External Information Class
#	This class will be responsible for the accurate detection of a potential collision. In the event
#	a collision is determined, it will interact with an accelerometer to determine the acceleration undergone
#	by the vehicle. From this acceleration, we will be able to determine the force the car experienced. This class
#	will then communicate this information to the main email notification class. 
#	Status: To be implemented...
#
