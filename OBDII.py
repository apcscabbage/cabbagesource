# This class is for handling the request-reply functions
# with the OBD-II device connected via bluetooth
# https://python-obd.readthedocs.io/en/latest/
# To install package ~~> $ pip install obd

import obd
import time

class Obd():
    def gather():
    	connection = obd.OBD() # auto connects to USB or RF port
        cmd = obd.commands.SPEED
        response = connection.query(cmd)
        print(response.value)
        print(response.value.to("mph"))

    def monitorSpeed():
    	connection = obd.OBD() # auto connects to USB or RF port
        cmd = obd.commands.SPEED
        lastSpeed = connection.query(cmd)
        while connection.status() == OBDStatus.CAR_CONNECTED:
        	currentSpeed = connection.query(cmd)
        	if currentSpeed.value.to("mph") - lastSpeed.value.to("mph") < -5:
        		#Communicate with accelerometer to confirm possibility for crash
        	else
        		lastSpeed = currentSpeed
        	time.sleep(2)
# Other ways to set up connections:
#
# connection = obd.OBD("valid USB/Bluetooth path") # create connection with USB 0
#
# OR
#
# ports = obd.scan_serial()      # return list of valid USB or RF ports
# print(ports)                    # ['/dev/ttyUSB0', '/dev/ttyUSB1']
# connection = obd.OBD(ports[0]) # connect to the first port in the list
